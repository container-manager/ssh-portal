package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/ssh"
)

type RhandConfig struct {
	GoShell     func(lhand io.ReadWriteCloser, ctx *ShellContext) error
	RhandTarget string
	RhandConfig *ssh.ClientConfig
	Context     *ShellContext
}

func getUser(key string) (*User, bool) {
	userKey := &UserKey{
		PublicKey: key,
	}
	db.Where(userKey).First(userKey)
	if userKey.UserID == 0 {
		return nil, false
	}

	expireDaysStr := os.Getenv("SSHPORTAL_KEYEXPIREDAYS")
	if expireDaysStr != "" {
		days, err := strconv.Atoi(expireDaysStr)
		if err != nil {
			panic(err)
		}
		expireDate := userKey.AccessedAt.AddDate(0, 0, days)
		if expireDate.Before(time.Now()) {
			return nil, false
		}
	}
	db.Model(userKey).Update("accessed_at", time.Now())

	user := &User{}
	user.ID = userKey.UserID
	db.Where(user).First(user)
	return user, userKey.IsToken
}

var hostAccessQuery = "SELECT id FROM `groups` " +
	"LEFT JOIN group_users AS u ON id = u.group_id " +
	"LEFT JOIN group_hosts AS h ON id = h.group_id " +
	"WHERE user_id = ? AND host_id = ?;"

var policyEvaluationQuery = "SELECT id FROM `groups` " +
	"LEFT JOIN group_users AS u ON id = u.group_id " +
	"LEFT JOIN group_remotes AS g ON id = g.group_id " +
	"WHERE (user_id = ? OR all_users = 1 ) and (remote_id = ? OR all_remotes = 1 );"

// TODO: hostkey
func getRemoteConfig(user *User, isToken bool, target string) (string, *ssh.ClientConfig, error) {
	remote := &Remote{}
	res := remote.FindOnePreferOwned(target, user.ID)
	if res == 0 {
		return "", nil, errors.New("No remote with name " + target)
	}
	key := &RemoteKey{}
	db.Model(remote).Related(key)
	if key.ID == 0 {
		return "", nil, errors.New("No key configured for " + target)
	}

	res = db.Raw(policyEvaluationQuery, user.ID, remote.ID).Scan(&gorm.Model{}).RowsAffected

	if res == 0 {
		return "", nil, errors.New("Access denied")
	}

	if remote.RequireToken && !isToken {
		return "", nil, errors.New("Remote requires a hardware token")
	}

	signer, err := ssh.ParsePrivateKey([]byte(key.Key))
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	db.Model(remote).Update("accessed_at", time.Now())

	username := remote.Username
	if username == "" {
		username = user.Name
	}
	target = fmt.Sprintf("%s:%d", remote.Hostname, remote.Port)
	config := &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	return target, config, nil
}

func GetTargetConfig(target string, key string) (*RhandConfig, error) {
	config := &RhandConfig{}
	user, isToken := getUser(key)

	context := &ShellContext{
		PublicKey: key,
		User:      user,
		IsToken:   isToken,
	}

	config.Context = context

	host := &Host{
		Name: hostname,
	}
	db.Find(host, host)

	if host.RestrictAccess == true && user.IsAdmin == false {
		res := db.Raw(hostAccessQuery, user.ID, host.ID).Scan(&gorm.Model{}).RowsAffected
		if res == 0 {
			return config, errors.New("This host is restricted")
		}
	}

	if val, ok := shellMap[target]; ok {
		config.GoShell = val
		return config, nil
	}
	if user == nil {
		return config, errors.New("User auth failed. Maybe your key expired or you need to register?")
	}
	target, cConfig, err := getRemoteConfig(user, isToken, target)

	if err != nil {
		return config, err
	}
	config.RhandTarget = target
	config.RhandConfig = cConfig
	return config, nil
}
