package main

import (
	"strconv"
	"testing"
)

func TestPolicy(t *testing.T) {
	logManager := makeTestingLogManager()
	logManager.updateTest(t)
	port := startTestingServer()
	defer stopTestingServer(port)
	portStr := strconv.Itoa(port)

	createTestingUser("admin", portStr, t)
	createTestingUser("user1", portStr, t)
	createTestingUser("user2", portStr, t)

	key := &RemoteKey{}
	db.First(key)
	remotePortInt, _ := getFreePort()
	remotePort := uint16(remotePortInt)
	remoteListener := newTestingSSHTarget(remotePort, []string{key.PublicKey}, t)
	defer remoteListener.Close()

	remoteName := "remote1"
	remote := &Remote{
		Port:        remotePort,
		Hostname:    "127.0.0.1",
		Name:        remoteName,
		RemoteKeyID: 1,
	}
	db.Create(remote)

	user1 := &User{}
	user1.ID = 2 // ID is indexed off 1
	db.Find(user1)

	failMessage := "Error: Access denied"
	successMessage := "I am remote"

	t.Run("No groups -> fail", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user1", portStr, failMessage, t)
	})

	group1 := &Group{}
	db.Create(group1)

	db.Model(&group1).Association("Remotes").Append(remote)

	t.Run("User not in group -> fail", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user1", portStr, failMessage, t)
	})

	db.Model(&group1).Association("Users").Append(user1)

	t.Run("User in group -> success", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user1", portStr, successMessage, t)
	})

	t.Run("Incorrect user -> fail", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user2", portStr, failMessage, t)
	})

	db.Model(group1).Update("all_users", true)

	t.Run("all_users -> success", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user2", portStr, successMessage, t)
	})

	db.Unscoped().Delete(group1)

	user2 := &User{}
	user1.ID = 3
	db.Find(user2)

	group2 := &Group{
		AllRemotes: true,
	}
	db.Create(group2)

	t.Run("all_remotes no users -> fail", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user2", portStr, failMessage, t)
	})

	db.Model(&group2).Association("Users").Append(user2)
	t.Run("all_remotes -> success", func(t *testing.T) {
		logManager.updateTest(t)
		testingEarlyExpect(remoteName, "user2", portStr, successMessage, t)
	})
}
