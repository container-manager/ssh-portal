package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"runtime/debug"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

func PortalServer(port uint16) net.Listener {
	banner := os.Getenv("SSHPORTAL_BANNER")
	config := &ssh.ServerConfig{
		PublicKeyCallback: func(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
			bytes := ssh.MarshalAuthorizedKey(key)
			lHandKey := string(bytes)
			lHandKey = strings.TrimSpace(lHandKey)
			permissions := &ssh.Permissions{
				Extensions: map[string]string{"lHandKey": lHandKey},
			}
			return permissions, nil
		},
		BannerCallback: func(conn ssh.ConnMetadata) string {
			host := &Host{
				Name: hostname,
			}
			db.First(host, host)
			res := banner + host.Message
			res = strings.Replace(res, "\\n", "\n", -1)
			res = strings.Replace(res, "\\r", "\r", -1)
			return res
		},
	}

	// use default key generated in migrate.go as the host key
	key := &RemoteKey{}
	db.First(key)
	private, err := ssh.ParsePrivateKey([]byte(key.Key))
	if err != nil {
		log.Fatal("Failed to parse private key")
	}
	config.AddHostKey(private)

	listenStr := fmt.Sprintf("0.0.0.0:%d", port)
	listener, err := net.Listen("tcp", listenStr)
	if err != nil {
		log.Fatalf("Failed to listen on %d (%s)", port, err)
	}

	// Accept all connections
	log.Printf("Listening on %d...", port)
	go func() {
		for {
			tcpConn, err := listener.Accept()
			if err != nil {
				log.Printf("Failed to accept incoming connection (%s)", err)
				return
			}

			// timeout new connections quickly
			// NewServerConn will block forever unless you do this
			timeoutConn := &TimeoutConn{
				Conn:        tcpConn,
				IdleTimeout: time.Second * 30,
			}

			go handleConnection(timeoutConn, config)
		}
	}()
	return listener
}

func handleConnection(tcpConn *TimeoutConn, sConfig *ssh.ServerConfig) {
	serverConn, chans, globalReqs, err := ssh.NewServerConn(tcpConn, sConfig)
	if err != nil {
		log.Printf("Failed to handshake (%s)", err)
		return
	}

	// timeout real ssh connections slowly
	tcpConn.IdleTimeout = time.Hour * 24

	log.Printf("New SSH connection from %s (%s)", serverConn.RemoteAddr(), serverConn.ClientVersion())
	target := serverConn.User()
	key := serverConn.Permissions.Extensions["lHandKey"]

	defer serverConn.Close()
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Panic in connection (%s)\n", target)
			fmt.Println(r)
			debug.PrintStack()
		}
	}()

	audit := &SSHAudit{
		Addr:   serverConn.RemoteAddr().String(),
		Target: target,
		Active: true,
	}
	db.Create(audit)
	defer db.Model(audit).Update("active", false)

	config, err := GetTargetConfig(target, key)
	if config.Context.User != nil {
		db.Model(audit).Update("username", config.Context.User.Name)
	}
	if err != nil {
		errStr := err.Error()
		writeClientError(chans, errStr)
		audit.Error = errStr
		return
	}

	if config.RhandConfig != nil {
		rhandConn, rhandChans, rhandReqs, err := connectRhand(config.RhandTarget, config.RhandConfig)
		if err != nil {
			errStr := err.Error()
			if _, ok := err.(net.Error); ok {
				errStr += "\r\nCheck that the target is running and that you are connecting to the correct host"
			}
			writeClientError(chans, errStr)
			db.Model(audit).Update("error", errStr)
			return
		}
		defer rhandConn.Close()
		go connReqHandler(globalReqs, rhandConn)
		go connReqHandler(rhandReqs, serverConn)
		go handleChannels(rhandChans, serverConn)
		handleChannels(chans, rhandConn)
	} else if config.GoShell != nil {
		go ssh.DiscardRequests(globalReqs)
		lhandChan, execStr, err := acceptOneChan(chans)
		config.Context.ExecStr = execStr
		if err != nil {
			errStr := err.Error()
			writeClientError(chans, errStr)
			db.Model(audit).Update("error", errStr)
			return
		}
		defer lhandChan.Close()
		err = config.GoShell(lhandChan, config.Context)
		exitStatus := byte(0)
		if err != nil {
			exitStatus = 1
		}
		lhandChan.SendRequest("exit-status", false, []byte{0, 0, 0, exitStatus})
		lhandChan.CloseWrite()
	} else {
		msg := "no targed named " + target
		writeClientError(chans, msg)
		db.Model(audit).Update("error", "no target")
	}
}

func connectRhand(target string, config *ssh.ClientConfig) (ssh.Conn, <-chan ssh.NewChannel, <-chan *ssh.Request, error) {
	nConn, err := net.DialTimeout("tcp", target, time.Second)
	if err != nil {
		return nil, nil, nil, err
	}
	return ssh.NewClientConn(nConn, target, config)
}

func handleChannels(chans <-chan ssh.NewChannel, rhand ssh.Conn) {
	for newChannel := range chans {
		go handleChannel(newChannel, rhand)
	}
}

func acceptOneChan(chans <-chan ssh.NewChannel) (ssh.Channel, string, error) {
	newChannel := <-chans
	lhandChan, reqs, err := newChannel.Accept()
	execStr := getExecReqString(reqs)
	go acceptAllReqs(reqs)
	go rejectAllChannels(chans)
	if err != nil {
		log.Printf("Could not accept channel (%s)", err)
		return nil, "", err
	}
	return lhandChan, execStr, nil
}

// getExecReqString returns empty string when shell and command when execRequest
func getExecReqString(reqs <-chan *ssh.Request) string {
	for req := range reqs {
		if req.WantReply {
			req.Reply(true, nil)
		}
		if req.Type == "exec" {
			// [4:] to index past header
			return string(req.Payload[4:])
		} else if req.Type == "shell" {
			return ""
		}
	}
	return ""
}

func acceptAllReqs(reqs <-chan *ssh.Request) {
	for req := range reqs {
		if req.WantReply {
			req.Reply(true, nil)
		}
	}
}

func rejectAllChannels(chans <-chan ssh.NewChannel) {
	for c := range chans {
		c.Reject(ssh.Prohibited, "")
	}
}

func writeClientError(chans <-chan ssh.NewChannel, msg string) {
	lhandChan, _, err := acceptOneChan(chans)
	if err != nil {
		return
	}
	lhandChan.Write([]byte("Error: " + msg + "\r\n"))
	lhandChan.Close()
	return
}

func connReqHandler(reqs <-chan *ssh.Request, target ssh.Conn) {
	for req := range reqs {
		ok, payload, _ := target.SendRequest(req.Type, req.WantReply, req.Payload)
		if req.WantReply {
			req.Reply(ok, payload)
		}
	}
}

func chanReqHandler(reqs <-chan *ssh.Request, target ssh.Channel) {
	for req := range reqs {
		ok, _ := target.SendRequest(req.Type, req.WantReply, req.Payload)
		if req.WantReply {
			req.Reply(ok, nil)
		}
	}
}

func handleChannel(newChannel ssh.NewChannel, destination ssh.Conn) {
	channelType := newChannel.ChannelType()
	extraData := newChannel.ExtraData()
	rhandChan, rhandReqs, err := destination.OpenChannel(channelType, extraData)
	if err != nil {
		newChannel.Reject(ssh.ConnectionFailed, err.Error())
		return
	}
	defer rhandChan.Close()

	lhandChan, lhandReqs, err := newChannel.Accept()
	if err != nil {
		log.Printf("Could not accept channel (%s)", err)
		return
	}
	defer lhandChan.Close()

	lhandStderr := lhandChan.Stderr()
	rhandStderr := rhandChan.Stderr()
	go io.Copy(lhandStderr, rhandStderr)
	go io.Copy(rhandStderr, lhandStderr)

	done := make(chan bool)

	go func() { chanReqHandler(rhandReqs, lhandChan); done <- true }()
	go func() { chanReqHandler(lhandReqs, rhandChan) }()
	go func() { io.Copy(lhandChan, rhandChan); lhandChan.CloseWrite(); done <- true }()
	go func() { io.Copy(rhandChan, lhandChan); rhandChan.CloseWrite() }()

	<-done
	<-done
}
