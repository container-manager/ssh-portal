package main

import (
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/fatih/color"
	"github.com/jinzhu/gorm"
	lxd "github.com/lxc/lxd/client"
	lxdShared "github.com/lxc/lxd/shared"
	"github.com/lxc/lxd/shared/api"
)

type ContainerAudit struct {
	CreatedAt time.Time
	Username  string
	Data      string
}

type ContainerImage struct {
	gorm.Model
	Name string `gorm:"not null;unique"`
}

type Container struct {
	gorm.Model
	Owner        *User `gorm:"foreignkey:OwnerID"`
	OwnerID      uint
	Remote       *Remote `gorm:"foreignkey:RemoteID"`
	RemoteID     uint
	ACL          *Group `gorm:"foreignkey:ACLID"`
	ACLID        uint
	Name         string
	InternalName string
	Image        string
	Host         string
	IP           string `gorm:"unique"`
	Status       string
	lxdConn      lxd.ContainerServer
}

func (c Container) String() string {
	return c.Name
}

type ContainerForward struct {
	CreatedAt   time.Time
	ID          uint  `gorm:"primary_key"`
	Owner       *User `gorm:"foreignkey:OwnerID"`
	OwnerID     uint
	Container   *Container `gorm:"foreignkey:ContainerID"`
	ContainerID uint
	Protocol    string `gorm:"unique_index:idx_forward"`
	LhandPort   uint16 `gorm:"unique_index:idx_forward"`
	RhandPort   uint16
	Comment     string
}

func newContainer(name string) *Container {
	ip := getNextIP()

	// create the database record as soon as possible to avoid race
	container := &Container{
		Name: name,
		IP:   ip,
		Host: hostname,
	}
	err := db.Create(container).Error
	if err != nil {
		panic(err)
	}
	internalName := fmt.Sprintf("c%d", container.ID)
	db.Model(container).Update("internal_name", internalName)
	return container
}

func (c *Container) findByNameID(name string, owner uint) int64 {
	return db.Where("name = ? and owner_id = ?", name, owner).First(c).RowsAffected
}

func (c *Container) findByInternalName(name string) int64 {
	return db.Where("internal_name = ?", name).First(c).RowsAffected
}

func (c *Container) connectToContainerServer(out io.Writer) (lxd.ContainerServer, error) {
	if c.lxdConn != nil {
		return c.lxdConn, nil
	}
	conn, err := connectToContainerServer(c.Host, out)
	c.lxdConn = conn
	if err == nil {
		printCheckmark(out)
	}
	return conn, err
}

func (c *Container) restart(force bool, out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	fmt.Fprint(out, "Restarting container")

	restartReq := api.ContainerStatePut{
		Action:  "restart",
		Force:   force,
		Timeout: 10,
	}

	op, err := server.UpdateContainerState(c.InternalName, restartReq, "")
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	return nil
}

func (c *Container) start(out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	fmt.Fprint(out, "Starting container")

	restartReq := api.ContainerStatePut{
		Action:  "start",
		Timeout: 10,
	}

	op, err := server.UpdateContainerState(c.InternalName, restartReq, "")
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	return nil
}

func (c *Container) rename(newName string, out io.Writer) error {
	currentRemote := &Remote{}
	db.Model(c).Related(currentRemote)
	oldName := currentRemote.Name

	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	fmt.Fprintf(out, "Renaming %s to %s", oldName, newName)

	newRemote := &Remote{}
	res := newRemote.FindOnePreferOwned(newName, c.OwnerID)
	if res != 0 {
		return errors.New("A container with that name already exists")
	}

	db.Model(currentRemote).Update("name", newName)
	db.Model(c).Update("name", newName)

	lxdContainer, etag, err := server.GetContainer(c.InternalName)
	lxdContainer.Config["user.remotename"] = newName
	op, err := server.UpdateContainer(c.InternalName, lxdContainer.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	printCheckmark(out)
	return nil
}

func (c *Container) copy(target string, out io.Writer) error {
	remote, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}
	local, err := connectToContainerServer(hostname, out)
	if err != nil {
		return err
	}
	printCheckmark(out)

	newContainer := newContainer(target)

	fmt.Fprint(out, "Stopping source container")
	stateReq := api.ContainerStatePut{
		Action:  "stop",
		Timeout: 30,
	}

	op, err := remote.UpdateContainerState(c.InternalName, stateReq, "")
	if err != nil {
		return err
	}
	err = op.Wait()
	if err != nil {
		printQmark(out)
		fmt.Fprintln(out, err)
	} else {
		printCheckmark(out)
	}

	containerHandle, _, _ := remote.GetContainer(c.InternalName)

	copyArgs := &lxd.ContainerCopyArgs{
		Name: newContainer.InternalName,
	}

	containerHandle.Devices["eth0"]["ipv4.address"] = newContainer.IP
	containerHandle.Config["user.remotename"] = target

	for key := range containerHandle.Config {
		if key == "volatile.eth0.hwaddr" {
			delete(containerHandle.Config, key)
		}
	}

	for key, dev := range containerHandle.Devices {
		dType := dev["type"]
		if dType == "disk" || dType == "gpu" || strings.HasSuffix(key, "-net") {
			delete(containerHandle.Devices, key)
		}
	}

	initialCopyMessage := "Copying container. This operation will continue even if you exit the session."
	fmt.Fprintf(out, initialCopyMessage)
	rop, err := local.CopyContainer(remote, *containerHandle, copyArgs)
	if err != nil {
		return err
	}

	progress := &ProgressRenderer{
		MaxLength: len(initialCopyMessage),
		Format:    "Copying container: %s",
		Out:       out,
	}

	rop.AddHandler(progress.UpdateOp)

	err = rop.Wait()
	if err != nil {
		return err
	}
	progress.Done()
	printCheckmark(out)

	fmt.Fprint(out, "Starting source container")
	stateReq.Action = "start"
	op, err = remote.UpdateContainerState(c.InternalName, stateReq, "")
	if err != nil {
		return err
	}
	err = op.Wait()
	if err != nil {
		return err
	}
	printCheckmark(out)

	fmt.Fprint(out, "Starting new container")
	stateReq.Action = "start"
	op, err = local.UpdateContainerState(newContainer.InternalName, stateReq, "")
	if err != nil {
		return err
	}
	err = op.Wait()
	if err != nil {
		return err
	}
	printCheckmark(out)

	fmt.Fprintf(out, "Creating new database records")

	remoteRecord := &Remote{
		Name:     target,
		Port:     22,
		Comment:  "@container",
		Hostname: newContainer.IP,
		OwnerID:  c.OwnerID,
	}
	db.Create(remoteRecord)

	owner := &User{}
	owner.ID = c.OwnerID

	acl := &Group{
		Comment: "@container",
	}
	db.Create(acl)
	d := db.Set("gorm:association_autoupdate", false)
	d.Model(acl).Association("Users").Append(owner)
	d.Model(acl).Association("Remotes").Append(remoteRecord)

	containerUpdates := &Container{
		Host:     hostname,
		Image:    c.Image,
		Status:   "running",
		OwnerID:  c.OwnerID,
		RemoteID: remoteRecord.ID,
		ACLID:    acl.ID,
	}
	db.Model(newContainer).Updates(containerUpdates)

	return nil
}

func (c *Container) migrate(out io.Writer) error {
	remote, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}
	local, err := connectToContainerServer(hostname, out)
	if err != nil {
		return err
	}
	printCheckmark(out)

	fmt.Fprint(out, "Stopping container")

	stateReq := api.ContainerStatePut{
		Action:  "stop",
		Timeout: 30,
	}

	op, err := remote.UpdateContainerState(c.InternalName, stateReq, "")
	if err != nil {
		return err
	}
	err = op.Wait()
	if err != nil {
		return err
	}
	printCheckmark(out)

	containerHandle, _, _ := remote.GetContainer(c.InternalName)

	for key, dev := range containerHandle.Devices {
		dType := dev["type"]
		if dType == "disk" || dType == "gpu" || strings.HasSuffix(key, "-net") {
			delete(containerHandle.Devices, key)
		}
	}

	initialCopyMessage := "Migrating container. This operation will continue even if you exit the session."
	fmt.Fprintf(out, initialCopyMessage)
	rop, err := local.CopyContainer(remote, *containerHandle, nil)
	if err != nil {
		return err
	}

	progress := &ProgressRenderer{
		MaxLength: len(initialCopyMessage),
		Format:    "Migrating container: %s",
		Out:       out,
	}

	rop.AddHandler(progress.UpdateOp)

	err = rop.Wait()
	if err != nil {
		return err
	}
	progress.Done()
	printCheckmark(out)

	db.Model(c).Update("host", hostname)

	fmt.Fprint(out, "Deleting source container")
	op, err = remote.DeleteContainer(c.InternalName)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	printCheckmark(out)

	fmt.Fprint(out, "Starting new container")
	stateReq.Action = "start"
	op, err = local.UpdateContainerState(c.InternalName, stateReq, "")
	if err != nil {
		return err
	}
	err = op.Wait()
	if err != nil {
		return err
	}

	printCheckmark(out)

	return nil
}

func (c *Container) remove(out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	fmt.Fprint(out, "Stopping container")

	req := api.ContainerStatePut{
		Action:  "stop",
		Timeout: -1,
		Force:   true,
	}

	op, err := server.UpdateContainerState(c.InternalName, req, "")
	if err != nil {
		return err
	}
	err = op.Wait()
	if err != nil {
		printX(out)
	} else {
		printCheckmark(out)
	}

	fmt.Fprint(out, "Deleting container")

	op, err = server.DeleteContainer(c.InternalName)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	printCheckmark(out)

	fmt.Fprint(out, "Removing database records")

	udb := db.Unscoped()
	udb.Delete(Remote{}, "id = ?", c.RemoteID)
	udb.Delete(Group{}, "id = ?", c.ACLID)
	udb.Delete(c)

	return nil
}

func (c *Container) populateStatus() {
	server, err := c.connectToContainerServer(ioutil.Discard)
	if err != nil {
		c.Status = "bad-server"
		return
	}

	cInfo, _, err := server.GetContainer(c.InternalName)
	if err != nil {
		c.Status = "not-found"
		return
	}
	c.Status = strings.ToLower(cInfo.Status)
}

type attachConfig struct {
	name     string
	local    bool
	nfs      bool
	readonly bool
	token    bool
	network  bool
}

func projectToAttachConfig(project *Group, userID uint) *attachConfig {
	config := &attachConfig{
		name: project.Name,
	}
	tags := strings.Split(project.Tags, ",")
	for _, tag := range tags {
		if tag == "local" {
			config.local = true
		} else if tag == "nfs" {
			config.nfs = true
		} else if tag == "readonly" {
			res := db.Raw(containersProjectIsAdminQuery, project.ID, userID).Scan(&gorm.Model{}).RowsAffected
			if res != 1 {
				config.readonly = true
			}
		} else if tag == "token" {
			config.token = true
		} else if tag == "network" {
			config.network = true
		}
	}
	return config
}

func getProjectAttachConfigs(projects []string, userID uint) ([]*attachConfig, error) {
	var attachConfigs []*attachConfig
	for _, projectName := range projects {
		project := &Group{}
		res := db.Raw(containersProjectQuery, projectName, userID).Scan(project).RowsAffected
		if res == 0 {
			return nil, fmt.Errorf("project %s is non existant or you don't have access ", projectName)
		}
		c := projectToAttachConfig(project, userID)
		attachConfigs = append(attachConfigs, c)
	}
	return attachConfigs, nil
}

func isCopyDisabled() bool {
	_, ok := os.LookupEnv("SSHPORTAL_ENABLE_COPY")
	return !ok
}

// getNextInterface finds the next avaliable interface for a container given it's devices
func getNextInterface(devs map[string]map[string]string) string {
	currentNames := make(map[string]bool)
	for _, v := range devs {
		if v["type"] != "nic" {
			continue
		}
		currentNames[v["name"]] = true
	}

	for i := 0; i < 50; i++ {
		name := fmt.Sprintf("eth%d", i)
		_, ok := currentNames[name]
		if !ok {
			return name
		}
	}
	panic("unable to find free interface")
}

func (c *Container) projectAttach(config *attachConfig, out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}
	name := config.name

	localName := fmt.Sprintf("%s-local", name)
	nfsName := fmt.Sprintf("%s-nfs", name)
	netName := fmt.Sprintf("%s-net", name)

	container, etag, err := server.GetContainer(c.InternalName)
	if err != nil {
		return err
	}

	if config.token {
		fmt.Fprintf(out, "Enforcing token requirement")

		remote := &Remote{}
		db.Model(c).Related(remote)
		db.Model(remote).Update("require_token", true)
		printCheckmark(out)
	}

	if config.local {
		fmt.Fprint(out, "Creating local volume")
		vol := api.StorageVolumesPost{
			Name: localName,
			Type: "custom",
		}
		err = server.CreateStoragePoolVolume("default", vol)
		if err != nil {
			printQmark(out)
			fmt.Fprintln(out, err)
		} else {
			printCheckmark(out)
		}
		localPath := "/local/" + name
		container.Devices[localName] = map[string]string{
			"type":     "disk",
			"pool":     "default",
			"path":     localPath,
			"source":   localName,
			"readonly": strconv.FormatBool(config.readonly),
		}
	}

	if config.nfs {
		nfsPath := "/nfs/" + name
		container.Devices[nfsName] = map[string]string{
			"type":      "disk",
			"path":      nfsPath,
			"source":    nfsPath,
			"recursive": "true",
			"readonly":  strconv.FormatBool(config.readonly),
		}
	}

	if config.network && c.Host != hostname {
		fmt.Fprintf(out, "Skipping network attach because host is not %s\n", c.Host)
	} else if config.network {
		configKey := fmt.Sprintf("%s-vlan", name)
		vlan := ConfigGet(configKey)
		if vlan != "" {
			container.Devices[netName] = map[string]string{
				"type":    "nic",
				"nictype": "macvlan",
				"parent":  getPrimaryHostInterface(),
				"name":    getNextInterface(container.Devices),
				"vlan":    vlan,
			}
		} else {
			fmt.Fprint(out, "Unable to find vlan for network")
			printX(out)
		}
	}

	fmt.Fprint(out, "Attaching project")

	op, err := server.UpdateContainer(c.InternalName, container.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	return nil
}

func (c *Container) projectDetach(name string, out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	localName := fmt.Sprintf("%s-local", name)
	nfsName := fmt.Sprintf("%s-nfs", name)
	netName := fmt.Sprintf("%s-net", name)

	container, etag, err := server.GetContainer(c.InternalName)
	if err != nil {
		return err
	}

	fmt.Fprint(out, "Detaching project")

	delete(container.Devices, localName)
	delete(container.Devices, nfsName)
	delete(container.Devices, netName)

	op, err := server.UpdateContainer(c.InternalName, container.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	return nil
}

func (c *Container) proxyCreate(proto string, lhand uint16, rhand uint16, out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	fmt.Fprint(out, "Creating forward device")

	container, etag, err := server.GetContainer(c.InternalName)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s(%d-%d)", proto, lhand, rhand)
	listenStr := fmt.Sprintf("%s::%d", proto, lhand)
	connectStr := fmt.Sprintf("%s::%d", proto, rhand)
	container.Devices[name] = map[string]string{
		"type":    "proxy",
		"listen":  listenStr,
		"connect": connectStr,
	}

	op, err := server.UpdateContainer(c.InternalName, container.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	return nil
}

func (c *Container) proxyRemove(proto string, lhand uint16, rhand uint16, out io.Writer) error {
	server, err := c.connectToContainerServer(out)
	if err != nil {
		return err
	}

	fmt.Fprint(out, "Removing forward device")

	container, etag, err := server.GetContainer(c.InternalName)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s(%d-%d)", proto, lhand, rhand)
	delete(container.Devices, name)

	op, err := server.UpdateContainer(c.InternalName, container.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	return nil
}

type ProgressRenderer struct {
	Out        io.Writer
	Format     string
	MaxLength  int
	inProgress bool
	lock       sync.Mutex
	done       bool
}

func (p *ProgressRenderer) Update(status string) {
	p.lock.Lock()
	defer p.lock.Unlock()

	if p.done {
		return
	}

	msg := "%s"
	if p.Format != "" {
		msg = p.Format
	}
	msg = fmt.Sprintf(msg, status)
	msgLen := len(msg)

	if msgLen < p.MaxLength {
		msg = msg + strings.Repeat(" ", p.MaxLength-msgLen)
	}

	outStr := "\r" + msg
	fmt.Fprint(p.Out, outStr)
	p.MaxLength = msgLen
}

func (p *ProgressRenderer) Done() {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.done = true
}

func (p *ProgressRenderer) UpdateOp(op api.Operation) {
	if op.Metadata == nil {
		return
	}

	for key, value := range op.Metadata {
		if !strings.HasSuffix(key, "_progress") {
			continue
		}

		p.Update(value.(string))
		break
	}
}

func enumerateOwnLxdCertificate() {
	certificate, err := lxdShared.GetRemoteCertificate("https://localhost:8443", "")
	if err != nil {
		fmt.Print("unable to enumerate own certificate " + err.Error())
		return
	}
	pemCert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certificate.Raw})
	stringCert := string(pemCert)

	configKey := fmt.Sprintf("%s-certificate", hostname)

	config := &Config{
		Key: configKey,
	}
	db.FirstOrCreate(config, config)
	config.Value = stringCert
	db.Save(config)
}

func setLxdCertificates() {
	config := &Config{
		Key: "lxd-client-pki",
	}
	db.FirstOrCreate(config, config)
	if config.Value == "" {
		certBytes, keyBytes, err := lxdShared.GenerateMemCert(true, true)
		if err != nil {
			panic(err)
		}
		config.Value = string(certBytes)
		config.ExtraValue = string(keyBytes)
		config.Protect = true
		db.Save(config)
	}
	server, err := connectToContainerServer("local", ioutil.Discard)
	if err != nil {
		panic(err)
	}

	certBlock, _ := pem.Decode([]byte(config.Value))

	cert := api.CertificatesPost{}
	cert.Certificate = base64.StdEncoding.EncodeToString(certBlock.Bytes)
	cert.Name = "lxd-client-pki"
	cert.Type = "client"

	err = server.CreateCertificate(cert)
	if err != nil {
		fmt.Println(err)
	}
}

func setLxdAuthorizedKeys() {
	server, err := connectToContainerServer("local", ioutil.Discard)
	if err != nil {
		panic(err)
	}

	profile, etag, err := server.GetProfile("managed-container")
	if err != nil {
		panic(err)
	}

	key := &RemoteKey{}
	db.First(key)

	profile.Config["user.authorized_keys"] = key.PublicKey
	err = server.UpdateProfile("managed-container", profile.Writable(), etag)
	if err != nil {
		panic(err)
	}
}

var validNameRegex *regexp.Regexp = regexp.MustCompile(`^[[:alnum:]][[:alnum:]\-]{0,61}[[:alnum:]]$`)

func isValidName(name string) error {
	if !validNameRegex.MatchString(name) {
		err := fmt.Errorf("name '%s' does not match %s", name, validNameRegex.String())
		return err
	}
	return nil
}

func printCheckmark(output io.Writer) {
	green := color.New(color.FgGreen)
	green.Fprintln(output, " ✓")
}

func printX(output io.Writer) {
	red := color.New(color.FgRed)
	red.Fprintln(output, " 𝙭")
}

func printQmark(output io.Writer) {
	yellow := color.New(color.FgYellow)
	yellow.Fprintln(output, " ?")
}

func incIP(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func getNextIP() string {
	var containers []Container
	db.Find(&containers)

	netStr := os.Getenv("SSHPORTAL_NET")
	ip, myNet, _ := net.ParseCIDR(netStr)
	incIP(ip) // .1
	incIP(ip) // .2

	//WARN this will include 255 on a /24
	for ; myNet.Contains(ip); incIP(ip) {
		ipStr := ip.String()
		ipExists := false
		for _, container := range containers {
			if container.IP == ipStr {
				ipExists = true
				break
			}
		}
		if !ipExists {
			return ipStr
		}
	}
	return ""
}

func getPrimaryHostInterface() string {
	cmd := exec.Command("ip", "route", "get", "8.8.8.8")
	outRaw, err := cmd.CombinedOutput()
	if err != nil {
		panic(err)
	}
	out := string(outRaw)
	parts := strings.Split(out, " ")
	fmt.Printf("%+v\n", parts)
	dev := parts[4]
	return dev
}

func connectToContainerServer(host string, out io.Writer) (lxd.ContainerServer, error) {
	fmt.Fprintf(out, "Connecting to %s", host)

	if host == hostname || host == "local" {
		return lxd.ConnectLXDUnix("", nil)
	}

	pkiConfig := &Config{
		Key: "lxd-client-pki",
	}
	db.Where(pkiConfig).First(pkiConfig)
	if pkiConfig.ID == 0 {
		return nil, fmt.Errorf("client certifiate not found")
	}

	serverCertKey := fmt.Sprintf("%s-certificate", host)
	serverCert := &Config{
		Key: serverCertKey,
	}
	db.Where(serverCert).First(serverCert)
	if serverCert.ID == 0 {
		return nil, fmt.Errorf("server certificate not enumerated")
	}

	args := &lxd.ConnectionArgs{}
	args.TLSClientCert = pkiConfig.Value
	args.TLSClientKey = pkiConfig.ExtraValue
	args.TLSServerCert = serverCert.Value
	// ensure no proxy during connection
	args.Proxy = func(req *http.Request) (*url.URL, error) { return nil, nil }

	target := fmt.Sprintf("https://%s:8443", host)
	server, err := lxd.ConnectLXD(target, args)
	if err != nil {
		panic(err)
	}
	return server, err
}

func createContainer(remoteName string, image string, user *User, projects []string, out io.Writer) error {
	projectAttachConfigs, err := getProjectAttachConfigs(projects, user.ID)
	if err != nil {
		return err
	}

	container := newContainer(remoteName)
	server, err := container.connectToContainerServer(out)
	if err != nil {
		return err
	}

	reqCreate := api.ContainersPost{
		Source: api.ContainerSource{
			Type:  "image",
			Alias: image,
		},
	}

	key := &RemoteKey{}
	db.First(key)

	reqCreate.Name = container.InternalName
	reqCreate.Profiles = []string{"default", "managed-container"}
	reqCreate.Config = map[string]string{
		"user.username":   user.Name,
		"user.remotename": remoteName,
	}

	fmt.Fprintf(out, "Creating container %s", remoteName)

	op, err := server.CreateContainer(reqCreate)
	if err != nil {
		db.Unscoped().Delete(container)
		return err
	}

	err = op.Wait()
	if err != nil {
		db.Unscoped().Delete(container)
		return err
	}
	printCheckmark(out)

	c, etag, _ := server.GetContainer(container.InternalName)

	c.Devices = map[string]map[string]string{
		"eth0": {
			"type":         "nic",
			"name":         "eth0",
			"nictype":      "bridged",
			"parent":       "lxdbr0",
			"ipv4.address": container.IP,
		},
	}
	op, err = server.UpdateContainer(container.InternalName, c.Writable(), etag)
	fmt.Fprint(out, "Configuring network")

	if err != nil {
		db.Unscoped().Delete(container)
		return err
	}

	err = op.Wait()
	if err != nil {
		db.Unscoped().Delete(container)
		return err
	}
	printCheckmark(out)

	if len(projectAttachConfigs) > 0 {
		fmt.Fprintln(out, "Attaching projects")
		for _, p := range projectAttachConfigs {
			err := container.projectAttach(p, out)
			if err == nil {
				printCheckmark(out)
			} else {
				printX(out)
			}
		}
	}
	fmt.Fprint(out, "Starting container")

	startReq := api.ContainerStatePut{
		Action:  "start",
		Timeout: -1,
	}

	op, err = server.UpdateContainerState(container.InternalName, startReq, "")
	if err != nil {
		return err
	}
	printCheckmark(out)

	fmt.Fprintf(out, "Creating database records")

	remote := &Remote{
		Name:     remoteName,
		Port:     22,
		Comment:  "@container",
		Hostname: container.IP,
		OwnerID:  user.ID,
	}
	db.Create(remote)

	acl := &Group{
		Comment: "@container",
	}
	db.Create(acl)
	d := db.Set("gorm:association_autoupdate", false)
	d.Model(acl).Association("Users").Append(user)
	d.Model(acl).Association("Remotes").Append(remote)

	containerUpdates := &Container{
		Host:     hostname,
		Image:    image,
		Status:   "running",
		OwnerID:  user.ID,
		RemoteID: remote.ID,
		ACLID:    acl.ID,
	}
	db.Model(container).Updates(containerUpdates)

	printCheckmark(out)
	fmt.Fprintf(out, "The container has been created. Access it via %s@%s\n", remoteName, hostname)
	fmt.Fprintln(out, "There is a short delay before it fully starts")

	return nil
}
