module ssh-portal

go 1.13

require (
	github.com/abiosoft/ishell v2.0.0+incompatible
	github.com/abiosoft/readline v0.0.0-20180607040430-155bce2042db
	github.com/alexflint/go-arg v1.3.0
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be
	github.com/fatih/color v1.9.0
	github.com/flosch/pongo2 v0.0.0-20200913210552-0d938eb266f3 // indirect
	github.com/flynn-archive/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/gartnera/go-ldap-client v0.0.0-20200129230455-d0bbb1214eda
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lxc/lxd v0.0.0-20200914164040-f8d02f77325c
	github.com/olekukonko/tablewriter v0.0.4
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/macaroon-bakery.v2 v2.2.0 // indirect
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5 // indirect
)
