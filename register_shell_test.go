package main

import (
	"strconv"
	"testing"
)

func TestRegister(t *testing.T) {
	logManager := makeTestingLogManager()
	logManager.updateTest(t)
	port := startTestingServer()
	defer stopTestingServer(port)
	portStr := strconv.Itoa(port)

	t.Run("Admin register", func(t *testing.T) {
		logManager.updateTest(t)
		createTestingUser("admin", portStr, t)
		user := &User{}
		user.ID = 1
		db.First(user)
		if user.ID == 0 {
			t.Fatal("Admin creation failed")
		}
		if user.IsAdmin == false {
			t.Fatal("Inital user is not admin")
		}
	})

	t.Run("User register", func(t *testing.T) {
		logManager.updateTest(t)
		createTestingUser("user", portStr, t)

		user := &User{}
		user.ID = 2
		db.First(user)
		if user.ID == 0 {
			t.Fatal("User creation failed")
		}
		if user.IsAdmin {
			t.Fatal("Standard user is admin")
		}
	})

	t.Run("Bad credentials rejected", func(t *testing.T) {
		logManager.updateTest(t)
		s := newTestingSSHExpect("register", "user2", portStr, t)
		defer s.Close()
		simpleExpect(s, "Username: ", t)
		s.Send("user2\r\n")
		simpleExpect(s, "Password: ", t)
		s.Send("badpassword\r\n")
		simpleExpect(s, "Authentication failed", t)
	})

}
