package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"reflect"
	"strings"
	"time"

	"github.com/abiosoft/ishell"
	"github.com/abiosoft/readline"
	"github.com/alexflint/go-arg"
	"github.com/anmitsu/go-shlex"
	"github.com/olekukonko/tablewriter"
)

type ShellContext struct {
	User      *User
	PublicKey string
	IsToken   bool
	Message   string
	ExecStr   string
	term      *Terminal
}

type ShellGlobalArgs struct {
	Display string `help:"Set display mode (table,csv,json)"`
	Order   string `help:"Set order string on query"`
	Limit   uint   `help:"Set limit on query"`
}

var shellMap map[string]func(lhand io.ReadWriteCloser, ctx *ShellContext) error

func RegisterShells() {
	shellMap = make(map[string]func(lhand io.ReadWriteCloser, ctx *ShellContext) error)

	shellMap["register"] = RegisterShell

	AdminShellInit()
	shellMap["admin"] = AdminShell

	ContainersShellInit()
	shellMap["containers"] = ContainersShell
}

func ShellCli(out io.Writer, ctx *ShellContext, commands []*ishell.Cmd, args []string) error {
	config := &readline.Config{
		FuncIsTerminal: func() bool { return false },
		FuncMakeRaw:    func() error { return nil },
		FuncExitRaw:    func() error { return nil },
		Stdout:         out,
	}
	shell := ishell.NewWithConfig(config)
	shell.Set("ctx", ctx)

	for _, cmd := range commands {
		shell.AddCmd(cmd)
	}
	shell.DeleteCmd("clear")

	// run shell
	err := shell.Process(args...)
	return err
}

func ShellTerminal(lhand io.ReadWriteCloser, ctx *ShellContext, commands []*ishell.Cmd, auditFn func(string, string)) error {
	term := NewTerminal(lhand, "")
	ctx.term = term

	if ctx.ExecStr != "" {
		execStr := ctx.ExecStr
		if auditFn != nil {
			auditFn(ctx.User.Name, execStr)
		}
		words, _ := shlex.Split(execStr, true)
		return ShellCli(term, ctx, commands, words)
	}
	if ctx.Message != "" {
		fmt.Fprintln(term, ctx.Message)
	}

	term.SetPrompt("> ")
	for {
		line, err := term.ReadLine()
		if err != nil {
			return nil
		}

		if auditFn != nil {
			auditFn(ctx.User.Name, line)
		}

		words, err := shlex.Split(line, true)
		if err != nil {
			fmt.Fprintf(term, "syntax error.\n")
		}

		if len(words) == 1 && strings.ToLower(words[0]) == "exit" {
			return nil
		}
		ShellCli(term, ctx, commands, words)
	}
}

func ShellArgs(c *ishell.Context, args interface{}, name string) (*ShellContext, error) {
	ctx := c.Get("ctx").(*ShellContext)
	config := arg.Config{Program: name}
	p, _ := arg.NewParser(config, args)
	err := p.Parse(c.Args)
	if err == arg.ErrHelp {
		p.WriteHelp(ctx.term)
		return nil, err
	}
	return ctx, err
}

func getReflectedStringVal(item interface{}, field string) string {
	v := reflect.ValueOf(item)
	f := reflect.Indirect(v).FieldByName(field)

	switch a := f.Interface().(type) {
	case time.Time:
		return a.Format("01/02/06 15:04")
	default:
		return fmt.Sprintf("%v", f)
	}
}

func ShellPrintReflectTable(format string, dataInterface interface{}, headers []string, output io.Writer) {
	if format == "table" || format == "" {
		table := tablewriter.NewWriter(output)
		table.SetHeader(headers)
		table.SetAlignment(tablewriter.ALIGN_LEFT)

		data := reflect.ValueOf(dataInterface)
		for i := 0; i < data.Len(); i++ {
			item := data.Index(i).Interface()
			var row []string
			for _, field := range headers {
				row = append(row, getReflectedStringVal(item, field))
			}
			table.Append(row)
		}
		table.Render()
	} else if format == "csv" {
		w := csv.NewWriter(output)
		w.Write(headers)

		data := reflect.ValueOf(dataInterface)
		for i := 0; i < data.Len(); i++ {
			item := data.Index(i).Interface()
			var row []string
			for _, field := range headers {
				row = append(row, getReflectedStringVal(item, field))
			}
			w.Write(row)
		}
		w.Flush()
	}
}
