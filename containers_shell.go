package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/abiosoft/ishell"
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
)

var containersLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List containers",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			All    bool `arg:"-a" help:"Show all containers"`
			Status bool `arg:"-s" help:"Show container status"`
		}
		ctx, err := ShellArgs(c, &args, "ls")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		headers := []string{"Remote", "Image", "Host"}
		var containers []*Container
		if args.All == true && user.IsAdmin == true {
			headers = append(headers, "Owner", "InternalName")
			db.Preload("Remote").Preload("Owner").Find(&containers)
		} else {
			db.Preload("Remote").Where("owner_id = ?", user.ID).Find(&containers)
		}

		if args.Status {
			for _, c := range containers {
				c.populateStatus()
			}
			headers = append(headers, "Status")
		}

		ShellPrintReflectTable(args.Display, containers, headers, ctx.term)
	},
}

var containersCreateCommand = &ishell.Cmd{
	Name: "create",
	Help: "Create containers",
	Func: func(c *ishell.Context) {
		var args struct {
			Name     string   `arg:"positional,required" help:"Name of the container"`
			Image    string   `arg:"required" help:"Image for the container"`
			Projects []string `arg:"--project,separate" help:"Project to attach"`
		}
		ctx, err := ShellArgs(c, &args, "create")
		if err != nil {
			c.Println(err)
			return
		}
		err = isValidName(args.Name)
		if err != nil {
			c.Println("Error: " + err.Error())
			return
		}

		user := ctx.User

		remote := &Remote{}
		res := remote.FindOnePreferOwned(args.Name, user.ID)
		if res != 0 {
			c.Println("Error: a container with that name already exists")
			return
		}

		err = createContainer(args.Name, args.Image, user, args.Projects, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
	},
}

var containersRenameCommand = &ishell.Cmd{
	Name: "rename",
	Help: "Rename containers",
	Func: func(c *ishell.Context) {
		var args struct {
			CurrentName string `arg:"positional,required" help:"Current name of the container"`
			NewName     string `arg:"positional,required" help:"New name for the container"`
		}
		ctx, err := ShellArgs(c, &args, "rename")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}
		container.findByNameID(args.CurrentName, user.ID)
		if container.ID == 0 {
			c.Println("Error: couldn't find a current container by that name")
			return
		}

		err = container.rename(args.NewName, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}

		c.Println("Container renamed. If you restart the container, the hostname will be changed")
	},
}

var containersCopyCommand = &ishell.Cmd{
	Name: "copy",
	Help: "Copy containers",
	Func: func(c *ishell.Context) {
		var args struct {
			SourceName string `arg:"positional,required" help:"Name of the existing container"`
			NewName    string `arg:"positional,required" help:"Name of the new container"`
		}
		ctx, err := ShellArgs(c, &args, "copy")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		err = isValidName(args.NewName)
		if err != nil {
			c.Println("Error: " + err.Error())
			return
		}

		container := &Container{}
		container.findByNameID(args.SourceName, user.ID)
		if container.ID == 0 {
			c.Println("Error: no source container by that name")
			return
		}

		newContainer := &Container{}
		newContainer.findByNameID(args.NewName, user.ID)
		if newContainer.ID != 0 {
			c.Println("Error: container with new name already exists")
			return
		}

		if isCopyDisabled() {
			c.Println(("Error: copy has been disabled"))
			return
		}

		err = container.copy(args.NewName, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			c.Println("Failures may leave the system in an inconsistent state. Please notify an admin so they can debug.")
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersMigrateCommand = &ishell.Cmd{
	Name: "migrate",
	Help: "Migrate containers to this host",
	Func: func(c *ishell.Context) {
		var args struct {
			Name  string `arg:"positional,required" help:"Name of the container"`
			Admin bool   `arg:"-a" help:"Enable admin"`
		}
		ctx, err := ShellArgs(c, &args, "migrate")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}
		if user.IsAdmin && args.Admin {
			container.findByInternalName(args.Name)
		} else {
			container.findByNameID(args.Name, user.ID)
		}
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		if container.Host == hostname {
			c.Println("Error: that container is already here")
			return
		}

		if isCopyDisabled() {
			c.Println(("Error: migrate has been disabled"))
			return
		}

		err = container.migrate(ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			c.Println("Migration fails may leave the system in an inconsistent state. Please notify an admin so they can debug.")
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersRestartCommand = &ishell.Cmd{
	Name: "restart",
	Help: "Restart containers",
	Func: func(c *ishell.Context) {
		var args struct {
			Name  string `arg:"positional,required" help:"Name of the container"`
			Admin bool   `arg:"-a" help:"Enable admin"`
			Force bool
		}
		ctx, err := ShellArgs(c, &args, "restart")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}

		if user.IsAdmin && args.Admin {
			container.findByInternalName(args.Name)
		} else {
			container.findByNameID(args.Name, user.ID)
		}
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		err = container.restart(args.Force, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersStartCommand = &ishell.Cmd{
	Name: "start",
	Help: "Start containers",
	Func: func(c *ishell.Context) {
		var args struct {
			Name  string `arg:"positional,required" help:"Name of the container"`
			Admin bool   `arg:"-a" help:"Enable admin"`
		}
		ctx, err := ShellArgs(c, &args, "start")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}
		if user.IsAdmin && args.Admin {
			container.findByInternalName(args.Name)
		} else {
			container.findByNameID(args.Name, user.ID)
		}
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		err = container.start(ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersRmCommand = &ishell.Cmd{
	Name: "rm",
	Help: "Remove containers",
	Func: func(c *ishell.Context) {
		var args struct {
			Name  string `arg:"positional,required" help:"Name of the container"`
			Admin bool   `arg:"-a" help:"Enable admin"`
		}
		ctx, err := ShellArgs(c, &args, "rm")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}
		if user.IsAdmin && args.Admin {
			container.findByInternalName(args.Name)
		} else {
			container.findByNameID(args.Name, user.ID)
		}
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		err = container.remove(ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersImages = &ishell.Cmd{
	Name: "images",
	Help: "List avaliable images",
	Func: func(c *ishell.Context) {
		ctx := c.Get("ctx").(*ShellContext)
		server, err := connectToContainerServer(hostname, ioutil.Discard)
		if err != nil {
			c.Println("Error: " + err.Error())
			return
		}
		names, err := server.GetImageAliasNames()
		if err != nil {
			c.Println("Error: " + err.Error())
			return
		}
		table := tablewriter.NewWriter(ctx.term)
		table.SetHeader([]string{"Name"})
		table.SetAlignment(tablewriter.ALIGN_LEFT)

		for i := 0; i < len(names); i++ {
			row := []string{names[i]}
			table.Append(row)
		}
		table.Render()
	},
}

var containersProjectQuery = "SELECT g.* FROM `groups` AS g " +
	"LEFT JOIN group_users as gu on g.id = gu.group_id " +
	"WHERE g.comment = '@project' AND g.name = ? " +
	"AND (gu.user_id = ? OR g.all_users = 1);"

var containersProjectIsAdminQuery = "SELECT id FROM `groups` AS g " +
	"LEFT JOIN group_admins as ga on g.id = ga.group_id " +
	"WHERE g.comment = '@project' AND g.name = ? AND ga.user_id = ?;"

var containersProjectCommand = &ishell.Cmd{
	Name: "project",
	Help: "Project operations",
}

var containersProjectLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List projects",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
		}
		ctx, err := ShellArgs(c, &args, "project ls")
		if err != nil {
			c.Println(err)
			return
		}

		headers := []string{"Name", "Tags"}
		var projects []*Group
		db.Where("comment = '@project'").Find(&projects)

		ShellPrintReflectTable(args.Display, projects, headers, ctx.term)
	},
}

var containersProjectAttachCommand = &ishell.Cmd{
	Name: "attach",
	Help: "Attach a container to a project",
	Func: func(c *ishell.Context) {
		var args struct {
			Name    string `arg:"positional,required" help:"Name of the container"`
			Project string `arg:"required" help:"Name of the project"`
		}
		ctx, err := ShellArgs(c, &args, "project attach")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}
		container.findByNameID(args.Name, user.ID)
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		project := &Group{}
		res := db.Raw(containersProjectQuery, args.Project, user.ID).Scan(project).RowsAffected
		if res == 0 {
			c.Println("Error: that project doesn't exist or you don't have access")
			return
		}

		config := projectToAttachConfig(project, user.ID)
		err = container.projectAttach(config, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
		if config.network {
			c.Println("Note: when attaching networks, IP addresses are not automatically obtained (solved by reboot)")
		}
	},
}

var containersProjectDetachCommand = &ishell.Cmd{
	Name: "detach",
	Help: "Detach a container from a project",
	Func: func(c *ishell.Context) {
		var args struct {
			Name    string `arg:"positional,required" help:"Name of the container"`
			Project string `arg:"required" help:"Name of the project"`
		}
		ctx, err := ShellArgs(c, &args, "project detach")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		container := &Container{}
		container.findByNameID(args.Name, user.ID)
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		err = container.projectDetach(args.Project, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
		c.Println("Project detached. You may have to restart your container to see the full effect.")
	},
}

var containersForwardCommand = &ishell.Cmd{
	Name: "forward",
	Help: "Port forward operations",
}

var containersForwardLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List port forwards",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			All bool `arg:"-a" help:"Show all forwards"`
		}
		ctx, err := ShellArgs(c, &args, "forward ls")
		if err != nil {
			c.Println(err)
			return
		}

		user := ctx.User

		headers := []string{"ID", "LhandPort", "RhandPort", "Protocol", "Comment", "Container"}
		var forwards []*ContainerForward
		query := db.Order("lhand_port asc").Preload("Container")
		if args.All {
			headers = append(headers, "Owner")
			query = query.Preload("Owner")
		} else {
			query = query.Where("owner_id = ?", user.ID)
		}
		query.Find(&forwards)

		ShellPrintReflectTable(args.Display, forwards, headers, ctx.term)
	},
}

var containersForwardCreateCommand = &ishell.Cmd{
	Name: "create",
	Help: "Create port forwards",
	Func: func(c *ishell.Context) {
		var args struct {
			Name     string `arg:"positional,required" help:"Name of the container"`
			Protocol string `arg:"required" help:"Protocol (tcp/udp)"`
			Lhand    uint16 `arg:"required" help:"Port on the host"`
			Rhand    uint16 `arg:"required" help:"Port on the container"`
			Comment  string `arg:"required" help:"Description of the forward"`
		}
		args.Protocol = "tcp"
		ctx, err := ShellArgs(c, &args, "forward create")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		if args.Lhand <= 1024 {
			c.Println("Error: lhand must be > 1024")
			return
		}

		container := &Container{}
		container.findByNameID(args.Name, user.ID)
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		forward := &ContainerForward{
			ContainerID: container.ID,
			OwnerID:     user.ID,
			Protocol:    args.Protocol,
			LhandPort:   args.Lhand,
			RhandPort:   args.Rhand,
			Comment:     args.Comment,
		}
		res := db.Create(forward).RowsAffected
		if res == 0 {
			c.Println("Error: a forward already exists on that port")
			return
		}

		err = container.proxyCreate(args.Protocol, args.Lhand, args.Rhand, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersForwardRmCommand = &ishell.Cmd{
	Name: "rm",
	Help: "Remove port forwards",
	Func: func(c *ishell.Context) {
		var args struct {
			ID uint `arg:"positional,required" help:"ID of the forward"`
		}
		ctx, err := ShellArgs(c, &args, "forward rm")
		if err != nil {
			c.Println(err)
			return
		}
		user := ctx.User

		forward := &ContainerForward{
			ID: args.ID,
		}
		db.Preload("Container").Find(forward, forward)
		if forward.LhandPort == 0 {
			c.Println("Error: that forward doesn't exist")
			return
		}

		if forward.OwnerID != user.ID && user.IsAdmin == false {
			c.Println("Error: you aren't allowed to delete that forward")
			return
		}

		db.Unscoped().Delete(forward)
		err = forward.Container.proxyRemove(forward.Protocol, forward.LhandPort, forward.RhandPort, ctx.term)
		if err != nil {
			printX(ctx.term)
			c.Println(err)
			return
		}
		printCheckmark(ctx.term)
	},
}

var containersShareCommand = &ishell.Cmd{
	Name: "share",
	Help: "Share a container with other users",
	Func: func(c *ishell.Context) {
		var args struct {
			Name     string `arg:"positional,required" help:"Name of the container"`
			Project  string `arg:"required" help:"Name of the project"`
			Username string `help:"User inside the container"`
		}
		ctx, err := ShellArgs(c, &args, "share")
		if err != nil {
			c.Println(err)
			return
		}

		user := ctx.User
		container := &Container{}
		container.findByNameID(args.Name, user.ID)
		if container.ID == 0 {
			c.Println("Error: no container by that name")
			return
		}

		project := &Group{}
		res := db.Raw(containersProjectQuery, args.Project, user.ID).Scan(project).RowsAffected
		if res == 0 {
			c.Println("Error: that project doesn't exist or you don't have access")
			return
		}

		remote := &Remote{}
		db.Model(container).Related(remote)
		db.Model(project).Association("Remotes").Append(remote)

		db.Model(remote).Association("Owner").Clear()
		db.Model(remote).Update("Username", args.Username)
		c.Println("Container is now shared")
	},
}

var ContainersShellCommands = []*ishell.Cmd{
	containersLsCommand,
	containersCreateCommand,
	containersRenameCommand,
	containersCopyCommand,
	containersImages,
	containersMigrateCommand,
	containersRestartCommand,
	containersStartCommand,
	containersRmCommand,
	containersProjectCommand,
	containersForwardCommand,
	containersShareCommand,
}

func ContainersShellInit() {
	db.AutoMigrate(&ContainerAudit{})
	db.AutoMigrate(&Container{})
	db.AutoMigrate(&ContainerImage{})
	db.AutoMigrate(&ContainerForward{})

	containersProjectCommand.AddCmd(containersProjectLsCommand)
	containersProjectCommand.AddCmd(containersProjectAttachCommand)
	containersProjectCommand.AddCmd(containersProjectDetachCommand)

	containersForwardCommand.AddCmd(containersForwardLsCommand)
	containersForwardCommand.AddCmd(containersForwardCreateCommand)
	containersForwardCommand.AddCmd(containersForwardRmCommand)

	setLxdAuthorizedKeys()
	enumerateOwnLxdCertificate()
	setLxdCertificates()
	color.NoColor = false
}

func ContainersShell(lhand io.ReadWriteCloser, ctx *ShellContext) error {
	if ctx.User == nil {
		fmt.Fprintln(lhand, "Error: Access denied\r")
		return errors.New("Access denied")
	}
	audit := func(username string, command string) {
		entry := &ContainerAudit{
			Username: username,
			Data:     command,
		}
		db.Create(entry)
	}
	return ShellTerminal(lhand, ctx, ContainersShellCommands, audit)
}
