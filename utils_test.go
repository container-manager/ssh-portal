package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/google/goexpect"
	"golang.org/x/crypto/ssh"
)

var testingServerShutdownChans map[int]chan bool

type testingLogManager struct {
	io.Writer
	mutex sync.Mutex
	t     *testing.T
}

func (w *testingLogManager) Write(buffer []byte) (int, error) {
	w.mutex.Lock()
	w.t.Log(string(buffer[:len(buffer)-1]))
	w.mutex.Unlock()
	return len(buffer), nil
}

func (w *testingLogManager) updateTest(t *testing.T) {
	w.mutex.Lock()
	w.t = t
	w.mutex.Unlock()
}

func makeTestingLogManager() *testingLogManager {
	w := &testingLogManager{}
	log.SetOutput(w)
	return w
}

func getFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}

func startTestingServer() int {
	port, _ := getFreePort()
	portStr := strconv.Itoa(port)
	dbPath := fmt.Sprintf("ssh-portal.%s.db", portStr)
	os.Remove(dbPath)
	os.Setenv("SSHPORTAL_TESTING", "true")
	os.Setenv("SSHPORTAL_PORT", portStr)
	os.Setenv("SSHPORTAL_SQLCONFIGSTRING", dbPath)

	if testingServerShutdownChans == nil {
		testingServerShutdownChans = make(map[int]chan bool)
	}

	c := make(chan bool)
	testingServerShutdownChans[port] = c

	go Start(c)
	time.Sleep(time.Second * 1)
	return port
}

func stopTestingServer(port int) {
	testingServerShutdownChans[port] <- true
}

// maps usernames to their keys
var testingKeyPairs map[string]*RemoteKey

func getTestingSigner(username string) ssh.Signer {
	if testingKeyPairs == nil {
		testingKeyPairs = make(map[string]*RemoteKey)
	}
	rKey, ok := testingKeyPairs[username]
	if !ok {
		rKey = GenerateRSAKeyPair()
		testingKeyPairs[username] = rKey
	}
	signer, _ := ssh.ParsePrivateKey([]byte(rKey.Key))
	return signer
}

func newTestingSSHClient(target string, username string, serverPort string) (*ssh.Client, error) {
	signer := getTestingSigner(username)
	config := &ssh.ClientConfig{
		User: target,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	client, err := ssh.Dial("tcp", "localhost:"+serverPort, config)
	if err != nil {
		return nil, err
	}
	return client, err
}

func newTestingSSHChannel(target string, username string, serverPort string, t *testing.T) (ssh.Channel, *ssh.Client) {
	client, err := newTestingSSHClient(target, username, serverPort)
	if err != nil {
		t.Fatal(err)
	}
	c, _, err := client.OpenChannel("shell", nil)
	if err != nil {
		t.Fatal(err)
	}

	return c, client
}

func newTestingSSHExpect(target string, username string, serverPort string, t *testing.T) *expect.GExpect {
	client, err := newTestingSSHClient(target, username, serverPort)
	if err != nil {
		t.Fatal(err)
	}
	e, _, err := expect.SpawnSSH(client, 5)
	if err != nil {
		t.Fatal("Failed to spawn ssh: ", err)
	}

	return e
}

func simpleExpect(e *expect.GExpect, val string, t *testing.T) {
	re := regexp.MustCompile(val)
	_, _, err := e.Expect(re, time.Second*5)
	if err != nil {
		t.Fatalf("Expect failed (%s): %s", val, err.Error())
	}
}

func newTestingSSHTarget(port uint16, keys []string, t *testing.T) net.Listener {
	config := &ssh.ServerConfig{
		PublicKeyCallback: func(c ssh.ConnMetadata, pubKey ssh.PublicKey) (*ssh.Permissions, error) {
			bytes := ssh.MarshalAuthorizedKey(pubKey)
			keyStr := string(bytes)
			keyStr = strings.TrimSpace(keyStr)
			for _, key := range keys {
				if key == keyStr {
					return nil, nil
				}
			}
			return nil, errors.New("unknown public key")
		},
	}

	hostKey := GenerateRSAKeyPair()
	private, err := ssh.ParsePrivateKey([]byte(hostKey.Key))
	if err != nil {
		t.Fatal("Failed to parse private key: ", err)
	}
	config.AddHostKey(private)

	listenStr := fmt.Sprintf("127.0.0.1:%d", port)
	listener, err := net.Listen("tcp", listenStr)
	if err != nil {
		t.Fatalf("Failed to listen on %d (%s)", port, err)
	}

	go func() {
		for {
			tcpConn, err := listener.Accept()
			if err != nil {
				return
			}
			serverConn, chans, reqs, err := ssh.NewServerConn(tcpConn, config)
			go ssh.DiscardRequests(reqs)
			if err != nil {
				t.Fatal(err)
			}
			newChannel := <-chans
			c, reqs, err := newChannel.Accept()
			if err != nil {
				t.Fatal(err)
			}
			go ssh.DiscardRequests(reqs)
			c.Write([]byte("I am remote\r\n"))
			time.Sleep(time.Millisecond * 250) // write and close are racy...
			c.Close()
			serverConn.Close()
		}
	}()

	return listener
}

func testingEarlyExpect(target string, username string, serverPort string, value string, t *testing.T) {
	sChan, sClient := newTestingSSHChannel(target, username, serverPort, t)
	defer sClient.Close()
	defer sChan.Close()
	sChan.SendRequest("shell", false, nil)
	buf := new(bytes.Buffer)
	buf.ReadFrom(sChan)
	result := buf.String()
	result = strings.TrimSpace(result)
	if result != value {
		t.Fatalf("expected %s got %s", value, result)
	}
}

func createTestingUser(username string, serverPort string, t *testing.T) {
	s := newTestingSSHExpect("register", username, serverPort, t)
	defer s.Close()
	simpleExpect(s, "Username: ", t)
	s.Send(username + "\r\n")
	simpleExpect(s, "Password: ", t)
	s.Send("test\r\n")
	simpleExpect(s, "Authentication success", t)

	// I can't expect "Keys registered" here for some reason, so I have to sleep instead
	time.Sleep(time.Millisecond * 250)
}
