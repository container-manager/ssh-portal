package main

import (
	"time"

	"github.com/jinzhu/gorm"
)

func Migrate() {
	db.AutoMigrate(&SSHAudit{})
	db.AutoMigrate(&User{})
	db.AutoMigrate(&UserKey{})
	db.AutoMigrate(&Remote{})
	db.AutoMigrate(&RemoteKey{})
	db.AutoMigrate(&Host{})
	db.AutoMigrate(&Group{})
	db.AutoMigrate(&Config{})

	// create default RemoteKey if it doesn't exist
	var remoteKeyCount int
	db.Model(&RemoteKey{}).Count(&remoteKeyCount)
	if remoteKeyCount == 0 {
		key := GenerateRSAKeyPair()

		db.Create(key)
	}
}

func FindByNameOrId(s interface{}, str string) int64 {
	return db.Where("id = ? or name = ?", str, str).Find(s).RowsAffected
}

func ConfigGet(key string) string {
	config := &Config{
		Key: key,
	}
	db.Where(config).First(config)
	return config.Value
}

func ConfigGetDefault(key string, def string) string {
	res := ConfigGet(key)
	if res == "" {
		return def
	}
	return res
}

type SSHAudit struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	Addr      string
	Target    string
	Username  string
	Error     string
	Active    bool
}

type User struct {
	gorm.Model `arg:"-"`
	Name       string `arg:"positional,required"`
	Comment    string
	IsAdmin    bool
	UserKeys   []UserKey `arg:"-"`
}

func (u User) String() string {
	return u.Name
}

func (u *User) BeforeDelete() {
	keys := &UserKey{
		UserID: u.ID,
	}
	db.Unscoped().Delete(keys, keys)
	db.Model(&Group{}).Association("Users").Delete(u)
}

type UserKey struct {
	gorm.Model
	AccessedAt *time.Time
	IsToken    bool
	Comment    string
	UserID     uint
	PublicKey  string `sql:"size:999999"`
}

type Remote struct {
	gorm.Model   `arg:"-"`
	AccessedAt   *time.Time `arg:"-"`
	Comment      string
	Name         string
	Username     string
	Hostname     string `arg:"positional,required"`
	Port         uint16
	RequireToken bool
	RemoteKey    *RemoteKey `gorm:"foreignkey:RemoteKeyID" arg:"-"`
	RemoteKeyID  uint       `gorm:"default:1"`
	Owner        *User      `gorm:"foreignkey:OwnerID" arg:"-"`
	OwnerID      uint
}

func (r *Remote) FindOnePreferOwned(name string, owner uint) int64 {
	return db.Where("(owner_id = ? OR owner_id = 0) AND name = ?", owner, name).Order("owner_id DESC").First(r).RowsAffected
}

func (r Remote) String() string {
	return r.Name
}

type RemoteKey struct {
	gorm.Model
	Comment   string
	PublicKey string `sql:"size:999999"`
	Key       string `sql:"size:999999"`
}

type Host struct {
	gorm.Model     `arg:"-"`
	Name           string `arg:"positional,required"`
	Message        string
	RestrictAccess bool
}

func (h Host) String() string {
	return h.Name
}

type Group struct {
	gorm.Model `arg:"-"`
	Name       string `arg:"positional,required"`
	Comment    string
	Admins     []User   `gorm:"many2many:group_admins;" arg:"-"`
	Users      []User   `gorm:"many2many:group_users;" arg:"-"`
	Remotes    []Remote `gorm:"many2many:group_remotes;" arg:"-"`
	Hosts      []Host   `gorm:"many2many:group_hosts;" arg:"-"`
	Tags       string   `help:"Comma separated tags"`
	AllUsers   bool
	AllRemotes bool
}

type Config struct {
	gorm.Model `arg:"-"`
	Key        string `gorm:"index"`
	Value      string `sql:"size:999999"`
	ExtraValue string `sql:"size:999999"`
	Protect    bool
}
