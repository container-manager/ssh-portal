package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"time"

	"github.com/abiosoft/ishell"
	"github.com/alexflint/go-arg"
)

type AdminAudit struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	Username  string
	Data      string
}

var adminUserCommand = &ishell.Cmd{
	Name: "user",
	Help: "User operations",
}

var adminUserLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List users",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			All bool `arg:"-a" help:"Show all users"`
		}
		ctx, err := ShellArgs(c, &args, "user ls")
		if err != nil {
			c.Println(err)
			return
		}

		var users []*User
		db.Find(&users)
		headers := []string{"ID", "Name", "Comment", "CreatedAt", "UpdatedAt", "IsAdmin"}
		ShellPrintReflectTable(args.Display, users, headers, ctx.term)
	},
}

var adminUserAddCommand = &ishell.Cmd{
	Name: "add",
	Help: "Add users",
	Func: func(c *ishell.Context) {
		user := &User{}
		ctx, err := ShellArgs(c, user, "user add")
		if err != nil {
			c.Println(err)
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		res := db.Create(user).RowsAffected
		c.Printf("%d rows affected\n", res)
	},
}

var adminUserUpdateCommand = &ishell.Cmd{
	Name: "update",
	Help: "Update users",
	Func: func(c *ishell.Context) {
		var args struct {
			Name       string `arg:"positional,required"`
			SetAdmin   bool
			UnsetAdmin bool
		}
		ctx, err := ShellArgs(c, &args, "user update")
		if err != nil {
			c.Println(err)
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		user := &User{}
		res := FindByNameOrId(user, args.Name)
		if res != 1 {
			c.Println("Error: invalid group name/id")
			return
		}
		if args.SetAdmin {
			db.Model(user).Update("is_admin", true)
		} else if args.UnsetAdmin {
			db.Model(user).Update("is_admin", false)
		}
	},
}

var adminRmUserCommand = &ishell.Cmd{
	Name: "rm",
	Help: "Remove users",
	Func: func(c *ishell.Context) {
		var args struct {
			Name string `arg:"positional,required" help:"The user name"`
		}
		ctx, err := ShellArgs(c, args, "user rm")
		if err != nil {
			c.Println(err)
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		user := &User{}
		res := FindByNameOrId(user, args.Name)
		if res != 1 {
			c.Println("Error: invalid user name/id")
			return
		}

		// delete all associated keys
		db.Unscoped().Where("user_id = ?", user.ID).Delete(UserKey{})

		res = db.Unscoped().Delete(user).RowsAffected
		c.Printf("%d rows affected\n", res)
	},
}

var adminRemoteCommand = &ishell.Cmd{
	Name: "remote",
	Help: "Remote operations",
}

var adminRemoteLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List remotes",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			All bool `arg:"-a" help:"Show all users"`
		}
		ctx, err := ShellArgs(c, &args, "user ls")
		if err != nil {
			c.Println(err)
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		var remotes []*Remote
		db.Preload("Owner").Find(&remotes)
		headers := []string{"ID", "Name", "CreatedAt", "UpdatedAt", "AccessedAt", "Hostname", "Port", "Username", "RemoteKeyID", "RequireToken", "Owner"}
		ShellPrintReflectTable(args.Display, remotes, headers, ctx.term)
	},
}

var adminRemoteAddCommand = &ishell.Cmd{
	Name: "add",
	Help: "Add remotes",
	Func: func(c *ishell.Context) {
		remote := &Remote{}
		remote.Port = 22
		remote.RemoteKeyID = 1

		ctx, err := ShellArgs(c, remote, "remote add")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		res := db.Create(remote).RowsAffected
		c.Printf("%d rows affected\n", res)
	},
}

var adminRemoteUpdateCommand = &ishell.Cmd{
	Name: "update",
	Help: "Update remotes",
	Func: func(c *ishell.Context) {
		var args struct {
			SetRequireToken   bool
			UnsetRequireToken bool
			Name              string `arg:"positional,required" help:"The remote name"`
		}
		ctx, err := ShellArgs(c, &args, "remote update")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}
		remote := &Remote{}
		res := FindByNameOrId(remote, args.Name)
		if res != 1 {
			c.Println("Error: invalid remote name/id")
			return
		}

		res = 0
		if args.SetRequireToken {
			res = db.Model(remote).Update("require_token", true).RowsAffected
		} else if args.UnsetRequireToken {
			res = db.Model(remote).Update("require_token", false).RowsAffected
		}

		c.Printf("%d rows affected\n", res)
	},
}

var adminRemoteRmCommand = &ishell.Cmd{
	Name: "rm",
	Help: "Remove remotes",
	Func: func(c *ishell.Context) {
		var args struct {
			Name string `arg:"positional,required" help:"The remote name"`
		}
		ctx, err := ShellArgs(c, &args, "remote rm")
		if err != nil {
			c.Println(err)
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		remote := &Remote{}
		res := FindByNameOrId(remote, args.Name)
		if res != 1 {
			c.Println("Error: invalid remote name/id")
			return
		}

		res = db.Unscoped().Delete(remote).RowsAffected
		c.Printf("%d rows affected\n", res)
	},
}

var adminRemoteKeyCommand = &ishell.Cmd{
	Name: "remotekey",
	Help: "Remote key operations",
}

var adminRemoteKeyLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List remote keys",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			All bool `arg:"-a" help:"Show all users"`
		}
		ctx, err := ShellArgs(c, &args, "remotekey ls")
		if err != nil {
			c.Println(err)
			return
		}

		var remoteKeys []*RemoteKey
		db.Find(&remoteKeys)
		headers := []string{"ID", "CreatedAt", "UpdatedAt", "PublicKey"}
		ShellPrintReflectTable(args.Display, remoteKeys, headers, ctx.term)
	},
}

var adminRemoteKeySetupCommand = &ishell.Cmd{
	Name: "setup",
	Help: "Print setup command for a key",
	Func: func(c *ishell.Context) {
		keyID := uint(1)
		if len(c.Args) == 1 {
			i, _ := strconv.Atoi(c.Args[0])
			keyID = uint(i)
		}

		key := &RemoteKey{}
		key.ID = keyID
		db.Find(key, key)
		c.Printf("umask 077; mkdir -p .ssh; echo %s >> .ssh/authorized_keys\n", key.PublicKey)
	},
}

var adminUserKeyCommand = &ishell.Cmd{
	Name: "userkey",
	Help: "User key operations",
}

var adminuserKeyLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List user keys",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			User string
		}
		ctx, err := ShellArgs(c, &args, "userkey ls")
		if err != nil {
			c.Println(err)
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		query := db
		if args.User != "" {
			user := &User{
				Name: args.User,
			}
			db.First(user, user)
			if user.ID != 0 {
				query = query.Where("user_id = ?", user.ID)
			}
		}

		var userKeys []*UserKey
		query.Find(&userKeys)
		headers := []string{"ID", "CreatedAt", "AccessedAt", "IsToken", "PublicKey"}
		ShellPrintReflectTable(args.Display, userKeys, headers, ctx.term)
	},
}

var adminuserKeyUpdateCommand = &ishell.Cmd{
	Name: "update",
	Help: "Update user keys",
	Func: func(c *ishell.Context) {
		var args struct {
			SetToken   bool
			UnsetToken bool
			ID         uint `arg:"positional,required"`
		}
		ctx, err := ShellArgs(c, &args, "userkey update")
		if err != nil {
			c.Println(err)
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		key := &UserKey{}
		key.ID = args.ID
		db.First(key, key)

		if key.PublicKey == "" {
			c.Println("Error: no key with that ID")
			return
		}

		isToken := args.SetToken
		res := db.Model(key).Update("is_token", isToken).RowsAffected
		c.Printf("%d rows affected\n", res)
	},
}

var adminGroupCommand = &ishell.Cmd{
	Name: "group",
	Help: "Group operations",
}

var adminGroupLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List groups",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
			Comment string `help:"Filter by comment"`
			Name    string `help:"Filter by name"`
		}
		ctx, err := ShellArgs(c, &args, "group ls")
		if err != nil {
			c.Println(err)
			return
		}

		groupFilter := Group{
			Comment: args.Comment,
			Name:    args.Name,
		}

		var groups []*Group
		db.Preload("Users").Preload("Remotes").Preload("Hosts").Preload("Admins").Find(&groups, groupFilter)
		headers := []string{"ID", "Name", "Comment", "CreatedAt", "UpdatedAt", "Users", "Remotes", "Hosts", "Admins", "Tags", "AllUsers", "AllRemotes"}
		ShellPrintReflectTable(args.Display, groups, headers, ctx.term)
	},
}

var adminGroupAddCommand = &ishell.Cmd{
	Name: "add",
	Help: "Add groups",
	Func: func(c *ishell.Context) {
		group := &Group{}

		ctx, err := ShellArgs(c, group, "group add")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		db.Create(group)
	},
}

var adminGroupRmCommand = &ishell.Cmd{
	Name: "rm",
	Help: "Remove groups",
	Func: func(c *ishell.Context) {
		var args struct {
			Name string `arg:"positional,required" help:"The group name"`
		}
		ctx, err := ShellArgs(c, &args, "remote rm")
		if err != nil {
			c.Println(err)
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		group := &Group{}
		res := FindByNameOrId(group, args.Name)
		if res != 1 {
			c.Println("Error: invalid group name/id")
			return
		}

		res = db.Unscoped().Delete(group).RowsAffected
		c.Printf("%d rows affected\n", res)
	},
}

var adminGroupUpdateCommand = &ishell.Cmd{
	Name: "update",
	Help: "update groups",
	Func: func(c *ishell.Context) {
		var args struct {
			Name            string `arg:"positional,required" help:"The group name"`
			AddAdmin        string
			RemoveAdmin     string
			AddUser         string
			RemoveUser      string
			AddRemote       string
			RemoveRemote    string
			AddHost         string
			RemoveHost      string
			Tags            string
			SetAllUsers     bool
			UnsetAllUsers   bool
			SetAllRemotes   bool
			UnsetAllRemotes bool
		}
		ctx, err := ShellArgs(c, &args, "group update")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}

		group := &Group{}
		res := FindByNameOrId(group, args.Name)
		if res != 1 {
			c.Println("Error: invalid group name/id")
			return
		}

		user := ctx.User
		groupAdmin := false

		if user.IsAdmin != true {
			admin := &User{}
			db.Model(group).Where("users.id = ?", user.ID).Related(&admin, "Admins")
			if admin.ID == 0 {
				c.Println("Error: admin required")
				return
			}
			groupAdmin = true
		}

		if args.AddAdmin != "" {
			user := &User{}
			res = FindByNameOrId(user, args.AddAdmin)
			if res != 1 {
				c.Println("Error: invalid user name")
				return
			}
			db.Model(group).Association("Admins").Append(user)
		}

		if args.RemoveAdmin != "" {
			user := &User{}
			res = FindByNameOrId(user, args.RemoveAdmin)
			if res != 1 {
				c.Println("Error: invalid user name")
				return
			}
			db.Model(group).Association("Admins").Delete(user)
		}

		if args.AddUser != "" {
			user := &User{}
			res = FindByNameOrId(user, args.AddUser)
			if res != 1 {
				c.Println("Error: invalid user name")
				return
			}
			db.Model(group).Association("Users").Append(user)
		}

		if args.RemoveUser != "" {
			user := &User{}
			res = FindByNameOrId(user, args.RemoveUser)
			if res != 1 {
				c.Println("Error: invalid user name")
				return
			}
			db.Model(group).Association("Users").Delete(user)
		}

		if args.AddRemote != "" {
			if groupAdmin {
				c.Println("Error: group admins are not allowed to add remotes")
				return
			}
			remote := &Remote{}
			res = FindByNameOrId(remote, args.AddRemote)
			if res != 1 {
				c.Println("Error: invalid remote name")
				return
			}
			db.Model(group).Association("Remotes").Append(remote)
		}

		if args.RemoveRemote != "" {
			remote := &Remote{}
			res = FindByNameOrId(remote, args.RemoveRemote)
			if res != 1 {
				c.Println("Error: invalid remote name")
				return
			}
			db.Model(group).Association("Remotes").Delete(remote)
		}

		if args.AddHost != "" {
			if groupAdmin {
				c.Println("Error: group admins are not allowed to add hosts")
				return
			}
			host := &Host{}
			res = FindByNameOrId(host, args.AddHost)
			if res != 1 {
				c.Println("Error: invalid host name")
				return
			}
			db.Model(group).Association("Hosts").Append(host)
		}

		if args.RemoveHost != "" {
			host := &Host{}
			res = FindByNameOrId(host, args.RemoveHost)
			if res != 1 {
				c.Println("Error: invalid host name")
				return
			}
			db.Model(group).Association("Hosts").Delete(host)
		}

		if args.SetAllUsers {
			db.Model(group).Update("all_users", true)
		} else if args.UnsetAllUsers {
			db.Model(group).Update("all_users", false)
		}

		if args.SetAllRemotes {
			if groupAdmin {
				c.Println("Error: group admins are not allowed to set all remotes")
				return
			}
			db.Model(group).Update("all_remotes", true)
		} else if args.UnsetAllRemotes {
			db.Model(group).Update("all_remotes", false)
		}

		if args.Tags != "" {
			db.Model(group).Update("tags", args.Tags)
		}
	},
}

var adminHostCommand = &ishell.Cmd{
	Name: "host",
	Help: "Host operations",
}

var adminHostLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List hosts",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
		}
		ctx, err := ShellArgs(c, &args, "host ls")
		if err != nil {
			c.Println(err)
			return
		}

		var hosts []*Host
		db.Find(&hosts)
		headers := []string{"ID", "Name", "RestrictAccess"}
		ShellPrintReflectTable(args.Display, hosts, headers, ctx.term)
	},
}

var adminHostAddCommand = &ishell.Cmd{
	Name: "add",
	Help: "Add hosts",
	Func: func(c *ishell.Context) {
		host := &Host{}

		ctx, err := ShellArgs(c, host, "host add")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}
		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		db.Create(host)
	},
}

var adminHostUpdateCommand = &ishell.Cmd{
	Name: "update",
	Help: "Update hosts",
	Func: func(c *ishell.Context) {
		var args struct {
			SetRestricted   bool
			UnsetRestricted bool
			Name            string `arg:"positional,required"`
		}

		ctx, err := ShellArgs(c, &args, "host update")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		host := &Host{}
		res := FindByNameOrId(host, args.Name)
		if res != 1 {
			c.Println("Error: invalid host name/id")
			return
		}

		if args.SetRestricted {
			db.Model(host).Update("restrict_access", true)
		} else if args.UnsetRestricted {
			db.Model(host).Update("restrict_access", false)
		}
	},
}

var adminHostRmCommand = &ishell.Cmd{
	Name: "rm",
	Help: "Remove hosts",
	Func: func(c *ishell.Context) {
		host := &Host{}

		ctx, err := ShellArgs(c, host, "host rm")
		if err != nil {
			if err != arg.ErrHelp {
				c.Println(err)
			}
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}

		db.Unscoped().Delete(host)
	},
}

var adminConfigCommand = &ishell.Cmd{
	Name: "config",
	Help: "Config operations",
}

var adminConfigSetCommand = &ishell.Cmd{
	Name: "set",
	Help: "Set config",
	Func: func(c *ishell.Context) {
		var args struct {
			Key   string `arg:"positional,required"`
			Value string `arg:"positional,required"`
		}
		ctx, err := ShellArgs(c, &args, "config set")
		if err != nil {
			c.Println(err)
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}
		config := &Config{
			Key: args.Key,
		}
		db.FirstOrCreate(config, config)
		config.Value = args.Value
		db.Save(config)
	},
}

var adminConfigGetCommand = &ishell.Cmd{
	Name: "get",
	Help: "Get config",
	Func: func(c *ishell.Context) {
		var args struct {
			Key string `arg:"positional,required"`
		}
		ctx, err := ShellArgs(c, &args, "config set")
		if err != nil {
			c.Println(err)
			return
		}

		if ctx.User.IsAdmin == false {
			c.Println("Error: admin required")
			return
		}
		config := &Config{
			Key: args.Key,
		}
		db.Find(config, config)
		if config.Value == "" {
			c.Println("Error: config not set")
			return
		}
		if config.Protect {
			c.Printf("Error: %s is protected\n", config.Key)
			return
		}
		c.Printf("%s:%s\n", config.Key, config.Value)
	},
}

var adminConfigLsCommand = &ishell.Cmd{
	Name: "ls",
	Help: "List configs",
	Func: func(c *ishell.Context) {
		var args struct {
			ShellGlobalArgs
		}
		ctx, err := ShellArgs(c, &args, "config ls")
		if err != nil {
			c.Println(err)
			return
		}

		var configs []*Config
		db.Find(&configs)
		for _, config := range configs {
			if config.Protect {
				if config.Value != "" {
					config.Value = "<protected>"
				}
				if config.ExtraValue != "" {
					config.ExtraValue = "<protected>"
				}
			}
		}
		headers := []string{"ID", "Key", "Value", "ExtraValue", "Protect"}
		ShellPrintReflectTable(args.Display, configs, headers, ctx.term)
	},
}

var AdminShellCommands = []*ishell.Cmd{
	adminUserCommand,
	adminRemoteCommand,
	adminRemoteKeyCommand,
	adminUserKeyCommand,
	adminGroupCommand,
	adminHostCommand,
	adminConfigCommand,
}

func AdminShellInit() {
	adminUserCommand.AddCmd(adminUserLsCommand)
	adminUserCommand.AddCmd(adminUserAddCommand)
	adminUserCommand.AddCmd(adminUserUpdateCommand)
	adminUserCommand.AddCmd(adminRmUserCommand)

	adminRemoteCommand.AddCmd(adminRemoteLsCommand)
	adminRemoteCommand.AddCmd(adminRemoteAddCommand)
	adminRemoteCommand.AddCmd(adminRemoteUpdateCommand)
	adminRemoteCommand.AddCmd(adminRemoteRmCommand)

	adminRemoteKeyCommand.AddCmd(adminRemoteKeyLsCommand)
	adminRemoteKeyCommand.AddCmd(adminRemoteKeySetupCommand)

	adminUserKeyCommand.AddCmd(adminuserKeyLsCommand)
	adminUserKeyCommand.AddCmd(adminuserKeyUpdateCommand)

	adminGroupCommand.AddCmd(adminGroupLsCommand)
	adminGroupCommand.AddCmd(adminGroupAddCommand)
	adminGroupCommand.AddCmd(adminGroupUpdateCommand)
	adminGroupCommand.AddCmd(adminGroupRmCommand)

	adminHostCommand.AddCmd(adminHostLsCommand)
	adminHostCommand.AddCmd(adminHostAddCommand)
	adminHostCommand.AddCmd(adminHostUpdateCommand)
	adminHostCommand.AddCmd(adminHostRmCommand)

	adminConfigCommand.AddCmd(adminConfigSetCommand)
	adminConfigCommand.AddCmd(adminConfigGetCommand)
	adminConfigCommand.AddCmd(adminConfigLsCommand)

	db.AutoMigrate(&AdminAudit{})
}

func AdminShell(lhand io.ReadWriteCloser, ctx *ShellContext) error {
	if ctx.User == nil {
		fmt.Fprintln(lhand, "Error: Access denied\r")
		return nil
	}

	// enforce that admin users are required to use tokens
	enforceAdminToken := os.Getenv("SSHPORTAL_ENFORCEADMINTOKEN")
	if enforceAdminToken != "" && ctx.User.IsAdmin && !ctx.IsToken {
		ctx.User.IsAdmin = false
		ctx.Message += "Error: Full admin requires a hardware token\r\n"
	}

	audit := func(username string, command string) {
		entry := &AdminAudit{
			Username: username,
			Data:     command,
		}
		db.Create(entry)
	}
	return ShellTerminal(lhand, ctx, AdminShellCommands, audit)
}
