package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gartnera/go-ldap-client"
	"golang.org/x/crypto/ssh/terminal"
)

func registerShellLdap(username string, password string) bool {
	ldapHost := ConfigGet("ldap_host")

	client := &ldap.LDAPClient{
		Base:         ConfigGet("ldap_base"),
		Host:         ldapHost,
		ServerName:   ldapHost,
		BindDN:       ConfigGet("ldap_bind_dn"),
		BindPassword: ConfigGet("ldap_bind_password"),
		Port:         636,
		UseSSL:       true,
		UserFilter:   "(uid=%s)",
		GroupFilter:  "(objectClass=%s)",
		Attributes:   []string{"givenName", "sn", "mail", "uid"},
	}
	defer client.Close()

	ok, _, err := client.Authenticate(username, password)
	if err != nil || ok == false {
		return false
	}
	return true
}

func registerShellAuthHandler(username string, password string) bool {
	isTesting := os.Getenv("SSHPORTAL_TESTING")
	if isTesting != "" {
		if password == "test" {
			return true
		}
		return false
	}
	return registerShellLdap(username, password)
}

func registerShellCreateRecord(username string, ctx *ShellContext) {
	user := &User{
		Name: username,
	}
	db.FirstOrCreate(user, user)

	// make the first user admin
	if user.ID == 1 {
		db.Model(user).Update("IsAdmin", true)
	}

	key := &UserKey{
		PublicKey: ctx.PublicKey,
	}

	db.FirstOrCreate(key, key)
	db.Model(key).Update("accessed_at", time.Now())

	db.Model(&user).Association("UserKeys").Append(key)
}

func registerShellGetUsername(term *terminal.Terminal) string {
	var username, password string
	var err error

	for {
		term.Write([]byte("Username: "))
		username, err = term.ReadLine()
		if err != nil {
			return ""
		}
		term.Write([]byte("Password: "))
		password, err = term.ReadPassword("")
		if err != nil {
			return ""
		}

		status := registerShellAuthHandler(username, password)
		if status {
			break
		}
		fmt.Fprintln(term, "Authentication failed")
	}

	return username
}

func RegisterShell(lhand io.ReadWriteCloser, ctx *ShellContext) error {
	term := terminal.NewTerminal(lhand, "")
	fmt.Fprintln(term, "Enter credentials to register your SSH keys.")

	defer lhand.Close()
	username := registerShellGetUsername(term)
	if username == "" {
		return nil
	}

	fmt.Fprintln(term, "Authentication success")
	registerShellCreateRecord(username, ctx)
	fmt.Fprintln(term, "Keys registered")
	return nil
}
