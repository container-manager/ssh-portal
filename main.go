package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kelseyhightower/envconfig"
)

type EnvConfig struct {
	Port            uint16 `default:"2222"`
	SQLDialect      string `default:"sqlite3"`
	SQLConfigString string `default:"ssh-portal.db"`
	DatabaseDebug   bool
}

var db *gorm.DB
var hostname string

func Start(shutdown chan bool) {
	var config = &EnvConfig{}
	err := envconfig.Process("sshportal", config)
	if err != nil {
		log.Fatal(err.Error())
	}

	db, err = gorm.Open(config.SQLDialect, config.SQLConfigString)
	if err != nil {
		log.Fatal(err.Error())
	}
	db.DB().SetConnMaxLifetime(time.Second * 10)
	defer db.Close()

	hostname, _ = os.Hostname()

	Migrate()
	RegisterShells()

	if config.DatabaseDebug == true {
		db = db.LogMode(true)
	}

	listener := PortalServer(config.Port)

	<-shutdown
	listener.Close()
}

func main() {
	shutdownChan := make(chan bool)
	go Start(shutdownChan)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGTERM)
	s := <-c
	log.Println("Got signal:", s)
	shutdownChan <- true
}
